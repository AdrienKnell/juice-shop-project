# Print K8s Master and Worker node IP
output "Master_Node_IP" {
  value = aws_instance.k8s_master_node_aws.public_ip
}
output "Worker_Node_IP" {
  value = aws_instance.k8s_worker_node_aws.public_ip
}
