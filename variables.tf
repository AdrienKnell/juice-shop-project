variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region"
}

variable "availability_zones" {
  default     = ["us-east-1a", "us-east-1b"]
  type        = list(any)
  description = "List of availability zones"
}
