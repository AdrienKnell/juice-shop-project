# Create Ansible Inventory
resource "local_file" "inventory" {
  depends_on = [
    aws_instance.k8s_master_node_aws,
    aws_instance.k8s_worker_node_aws,
  ]
  content  = "[Master]\n${aws_instance.k8s_master_node_aws.public_ip}\n\n[Worker]\n${aws_instance.k8s_worker_node_aws.public_ip}"
  filename = "inventory"
}

# Run Ansible playbook
resource "null_resource" "null_resource_name" {
  depends_on = [
    local_file.inventory
  ]

  provisioner "local-exec" {
    command = "ansible-playbook playbook.yml"
  }
}
